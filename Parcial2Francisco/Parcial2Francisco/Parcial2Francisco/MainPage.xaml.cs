﻿using Parcial2Francisco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Parcial2Francisco
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        //METODO PASAR MOSTRAR PERSONAS
        async private void PasarBuscar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new BuscarPersonas());    
        }
        //FIN PASAR MOSTRAR PERSONAS
        
        //
        async private void Lista(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarPersonas());
        }
        //

        //INICIO DE METODO GUARDAR
        async private void CrearPersona(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(entryNombres.Text) || string.IsNullOrEmpty(entryCedula.Text)|| string.IsNullOrEmpty(entryApellidos.Text))
            {
                await DisplayAlert("Error","Debe digitar todos los campos","Ok");
            }
            else {

                Personas persona = new Personas()
                {
                    Cedula = Convert.ToDouble(entryCedula.Text),
                    Nombres = entryNombres.Text,
                    Apellidos = entryApellidos.Text,
                    Fecha_nacimiento = pickerFecha.Date

                };

                using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
                {
                    conecction.CreateTable<Personas>();

                    var result = conecction.Insert(persona);
                    if (result > 0)
                    {
                      await  DisplayAlert("Correcto", "El usuario fue creado", "Ok");
                    }
                    else
                    {
                      await DisplayAlert("Error", "El usuario no fue agregado", "Ok");
                    }

                }

                entryNombres.Text = "";
                entryApellidos.Text = "";
                entryCedula.Text = "";

                await Navigation.PushAsync(new ListarPersonas());

                
            }
        }
        //FIN DE METODO GUARDAR
        
    }
}
