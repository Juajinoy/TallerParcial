﻿using Parcial2Francisco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Parcial2Francisco
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListarPersonas : ContentPage
	{
		public ListarPersonas ()
		{
			InitializeComponent ();
		}

        //INICIO DE LISTAR PERSONAS
        protected override void OnAppearing()
        {
            base.OnAppearing();
            using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
            {
                List<Personas> listaPersonas;
                listaPersonas = conecction.Table<Personas>().ToList();
                ListPersonas.ItemsSource = listaPersonas;
            }
        }
        //FIN DE LISTAR PERSONAS

        //INICIO DE BOTON REGRESAR A FORMULARIO
        async private void MostrarFormulario(object sender, EventArgs e)
        {
            
            await Navigation.PushAsync(new MainPage());
        }
        //FIN DE BOTON REGRESAR A FORMULARIO
    }
}