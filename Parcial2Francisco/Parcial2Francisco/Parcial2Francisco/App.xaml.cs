﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace Parcial2Francisco
{
	public partial class App : Application
	{
        // URL

        public static string urlBd;
		public App ()
		{
			InitializeComponent();

            //MainPage = new Parcial2Francisco.MainPage();
            MainPage = new NavigationPage(new MainPage());
		}

        //INICIO DE CONFIGURACION PARA BASE DE DATOS
        public App(string url)
        {
            InitializeComponent();
            urlBd = url;
            MainPage = new NavigationPage(new MainPage());
        }
        //FIN DE CONFIGURACION PARA BASE DE DATOS



        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
