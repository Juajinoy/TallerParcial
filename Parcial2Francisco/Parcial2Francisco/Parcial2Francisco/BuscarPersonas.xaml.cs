﻿using Parcial2Francisco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Parcial2Francisco
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BuscarPersonas : ContentPage
	{
		public BuscarPersonas ()
		{
			InitializeComponent ();
		}

        //INICIO DE METODO DE REGRESAR
        async private void regresar(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new MainPage());
        }
        //FIN DE METODO DE REGRESAR

        //INICIO DE METODO BUSCAR
        public void BuscarPersona()
        {

            if (string.IsNullOrEmpty(EntryCedula.Text))
            {
                DisplayAlert("Error", "Debe digitar el número de cedula", "Ok");
            }
            else {
                double ced = Convert.ToDouble(EntryCedula.Text);

                using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
                {
                    var listPer = conecction.Table<Personas>();
                    string n = null;
                    foreach (var aux in listPer) {
                        if (aux.Cedula == ced) {
                            labelNombre.Text = aux.Nombres.ToString();
                            labelApellidos.Text = aux.Apellidos.ToString();
                            labelFecha.Text = aux.Fecha_nacimiento.ToString();
                            n = "encontrado";
                        }

                    }
                    if (n == null) {
                        DisplayAlert("Advertencia", "El usuario no fue encontrado", "Ok");
                    }
                }
            }
        }
        //FIN DE METODO BUSCAR

        //INICIO DE METODO ELIMINAR
        public void Eliminar()
        {

            if (string.IsNullOrEmpty(EntryCedula.Text))
            {
                DisplayAlert("Error", "Debe digitar el número de cedula", "Ok");
            }
            else
            {
                double ced = Convert.ToDouble(EntryCedula.Text);

                using (SQLite.SQLiteConnection conecction = new SQLite.SQLiteConnection(App.urlBd))
                {
                    var listPer = conecction.Table<Personas>().FirstOrDefault(p => p.Cedula == ced);
                    string n = null;
                   if (listPer != null)
                        {
                        conecction.Delete(listPer);
                        n = "encontrado";
                        labelNombre.Text = "";
                        labelApellidos.Text = "";
                        labelFecha.Text = "";
                        EntryCedula.Text = "";

                        DisplayAlert("Advertencia","El usuario ha sido eliminado","Ok");
                        }
                    if (n == null)
                    {
                        DisplayAlert("Advertencia", "El usuario no fue encontrado", "Ok");
                    }
                }
            }
        }
        //FIN DE METODO ELIMINAR

        //INICIO METODO LIMPIAR
        public void Limpiar()
        {
            if (string.IsNullOrEmpty(labelApellidos.Text) || string.IsNullOrEmpty(labelNombre.Text) || string.IsNullOrEmpty(EntryCedula.Text)
                || string.IsNullOrEmpty(labelFecha.Text)) {

                DisplayAlert("Advertencia","Los campos estan vacios","Ok");
            }
            else { 
            labelNombre.Text = "";
            labelFecha.Text = "";
            labelApellidos.Text = "";
            EntryCedula.Text = "";
            }
        }
        //FIN METODO LIMPIAR
    }
}