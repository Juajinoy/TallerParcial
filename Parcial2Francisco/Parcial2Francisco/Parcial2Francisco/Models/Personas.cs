﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Parcial2Francisco.Models
{
    class Personas
    {
        [PrimaryKey, AutoIncrement, Column("_id") ]
        public int Id { get; set; }
        public double Cedula { get; set; }
        [MaxLength(150)]
        public string Nombres { get; set; }
        [MaxLength(150)]
        public string Apellidos { get; set; }
        public DateTime Fecha_nacimiento { get; set; }
    }
}
